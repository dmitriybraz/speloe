"use strict";
$(document).ready(function () {
	$(".main_slider").slick({
		fade: true,
		arrows: false,
		dots: true,
	});

	$(window).scroll(function () {
		if ($(window).scrollTop() > 200) {
			$(".menu_fixed").addClass("menu_fixed_show");
		} else {
			$(".menu_fixed").removeClass("menu_fixed_show");
		}
	});

	$(".menu_btn").on("click", function (e) {
		e.preventDefault();
		$(".m_menu").toggleClass("menu_active");
		$(".menu_btn").toggleClass("menu_btn_active");
		$(".m_menu_back").toggleClass("m_menu_back_active");
		$(".menu_btn i").toggleClass("m_menu_btn_show");
	});

	$(".m_menu_back").on("click", function () {
		$(".m_menu").toggleClass("menu_active");
		$(".menu_btn").toggleClass("menu_btn_active");
		$(".m_menu_back").toggleClass("m_menu_back_active");
		$(".menu_btn i").toggleClass("m_menu_btn_show");
	});

	$(function () {
		$(".tel").mask("+7 (999) 999-99-99");
	});

	$(".mobile_nav_drop.collapse").on("show.bs.collapse", function (e) {
		let a = $(e.target);
		let b = $(a).parent();
		b.addClass("mobile_nav_drop_link_active");
	});
	$(".mobile_nav_drop.collapse").on("hide.bs.collapse", function (e) {
		let a = $(e.target);
		let b = $(a).parent();
		b.removeClass("mobile_nav_drop_link_active");
	});

	$(".chldr_b6_frame .collapse").on("show.bs.collapse", function (e) {
		let a = $(e.target);
		let b = $(a).parent();
		let c = $(b).children(".chl_b6_link");
		c.html("Свернуть...");
	});
	$(".chldr_b6_frame .collapse").on("hide.bs.collapse", function (e) {
		let a = $(e.target);
		let b = $(a).parent();
		let c = $(b).children(".chl_b6_link");
		c.html("Развернуть...");
	});

	$("#chl_b6_other").on("show.bs.collapse", function (e) {
		$(".other_tariffs").html("Свернуть");
		$(".tariff_arrow_l").addClass("tariff_arrow_act_l");
		$(".tariff_arrow_r").addClass("tariff_arrow_act_r");
	});
	$("#chl_b6_other").on("hide.bs.collapse", function (e) {
		$(".other_tariffs").html("Посмотреть все тарифы");
		$(".tariff_arrow_l").removeClass("tariff_arrow_act_l");
		$(".tariff_arrow_r").removeClass("tariff_arrow_act_r");
	});

	$(".ab_b1_slider").slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: true,
		fade: true,
		nextArrow:
			'<button type="button" class="slick-next"><img src="./img/ab_arr_right.png" alt=""></button>',
		prevArrow:
			'<button type="button" class="slick-prev"><img src="./img/ab_arr_left.png" alt=""></button>',

		asNavFor: ".ab_b1_sldr_nav",
		responsive: [
			{
				breakpoint: 992,
				settings: {
					arrows: false,
				},
			},
		],
	});
	$(".ab_b1_sldr_nav").slick({
		slidesToShow: 3,
		slidesToScroll: 1,
		asNavFor: ".ab_b1_slider",
		dots: true,
		centerMode: true,
		arrows: false,
		focusOnSelect: true,
	});

	$(".ab_b2_slider").slick({
		infinite: true,
		slidesToShow: 4,
		slidesToScroll: 1,
		dots: true,
		arrows: true,
		nextArrow:
			'<button type="button" class="slick-next"><img class="vid_arr_nx" src="./img/ab_b2_arrnx.png" alt=""></button>',
		prevArrow:
			'<button type="button" class="slick-prev"><img class="vid_arr_pr" src="./img/ab_b2_arrpr.png" alt=""></button>',
		responsive: [
			{
				breakpoint: 992,
				settings: {
					arrows: false,
				},
			},
		],
	});

	$(".collapse_faq").on("show.bs.collapse", function (e) {
		let a = $(e.target);
		let b = $(a).parent();
		b.addClass("ab_b3_frame_active");
	});
	$(".collapse_faq").on("hide.bs.collapse", function (e) {
		let a = $(e.target);
		let b = $(a).parent();
		b.removeClass("ab_b3_frame_active");
	});
	$(".step5_slider").slick({
		infinite: true,
		slidesToShow: 4,
		slidesToScroll: 1,
		arrows: true,
		nextArrow:
			'<button type="button" class="slick-next"><i class="fa fa-chevron-right" aria-hidden="true"></i></button>',
		prevArrow:
			'<button type="button" class="slick-prev"><i class="fa fa-chevron-left" aria-hidden="true"></i></button>',
	});
});
document.addEventListener("DOMContentLoaded", () => {
	const wizard = new Zangdar("#calc_form", {
		onStepChange() {
			const breadcrumb = this.getBreadcrumb();
			buildSteps(breadcrumb);
		},
		onSubmit(e) {
			e.preventDefault();
			this.getCurrentStep().active = false;
			this.getCurrentStep().completed = true;
			const breadcrumb = this.getBreadcrumb();
			buildSteps(breadcrumb);
			e.target.style.display = "none";
			document.getElementById("form-completed").style.display = "block";
			return false;
		},
	});

	const buildBreadcrumb = () => {
		const breadcrumb = wizard.getBreadcrumb();
		buildSteps(breadcrumb);
	};

	const addStep = () => {
		const stepNumber = wizard.steps.length + 1;
		const html = `<section data-step="step-${stepNumber}">
		<h2>Step #${stepNumber}</h2>
		<div>
		  <button class="btn btn-primary float-right" data-next>Next</button>
		</div>
	  </section>`;
		wizard.getFormElement().innerHTML += html;
		wizard.refresh();
		buildBreadcrumb();
	};

	const removeStep = (index) => {
		wizard.removeStep(index);
		buildBreadcrumb();
	};

	const buildSteps = (steps) => {
		const $steps = document.getElementById("steps");

		if (document.getElementById("add__step") !== null)
			document
				.getElementById("add__step")
				.removeEventListener("click", addStep);

		if (document.getElementById("remove__step") !== null)
			document
				.getElementById("remove__step")
				.removeEventListener("click", removeStep);

		$steps.innerHTML = "";

		// let $li = document.createElement("li");
		// const $removeBtn = document.createElement("button");
		// $removeBtn.setAttribute("id", "remove__step");
		// $removeBtn.classList.add("btn", "btn-error");
		// $removeBtn.addEventListener("click", () => {
		// 	const index = wizard.steps.length - 1;
		// 	if (index < 3) return;
		// 	removeStep(index);
		// });
		// $removeBtn.innerHTML = "-";
		// $li.appendChild($removeBtn);
		// $steps.appendChild($li);

		for (let label in steps) {
			if (steps.hasOwnProperty(label)) {
				const $li = document.createElement("li");
				const $a = document.createElement("a");
				$li.classList.add("step-item");
				if (steps[label].active) {
					$li.classList.add("active");
				}
				$a.setAttribute("href", "#");
				$a.classList.add("tooltip");
				$a.dataset.tooltip = label;
				$a.innerText = label;
				$a.addEventListener("click", (e) => {
					e.preventDefault();
					wizard.revealStep(label);
				});
				$li.appendChild($a);
				$steps.appendChild($li);
			}
		}

		// $li = document.createElement("li");
		// const $addBtn = document.createElement("button");
		// $addBtn.setAttribute("id", "add__step");
		// $addBtn.classList.add("btn", "btn-primary");
		// $addBtn.addEventListener("click", addStep);
		// $addBtn.innerHTML = "+";
		// $li.appendChild($addBtn);
		// $steps.appendChild($li);
	};

	buildBreadcrumb();
});
